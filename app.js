//instance create
const github = new Github();

const ui = new UI();

//search key
const searchKey = document.getElementById('searchUser');

searchKey.addEventListener('keyup', (e) => {
    const textValue = e.target.value;
    if(textValue !== ''){
        github.getUser(textValue)
            .then(data => {
                if(data.profile.message == 'Not Found'){
                    //show not found alert
                    ui.showAlert("User not found", "alert alert-danger");
                } else {
                    //show profile
                    ui.profileView(data.profile);
                    ui.responseRepos(data.repo);
                }
            });

    } else {
        //clear the profile
        ui.clearProfile();
    }
    
})