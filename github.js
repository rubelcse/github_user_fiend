class Github {
    constructor(){
        this.client_id = '0145af5ec4e69dc772bd';
        this.client_secrate = '717aa28b6ff377c4ef59ff882c3d8821391acb63';
        this.limit = 5;
        this.sort_by = 'Created: Asc';
    }

    async getUser(user) {
        const responseText = await fetch(`https://api.github.com/users/${user}?client_id=${this.client_id}&client_secret=${this.client_secrate}`);
        const profile = await responseText.json();

        const repoResponse = await fetch(`https://api.github.com/users/${user}/repos?per_page=${this.limit}&sort=${this.sort_by}
            client_id=${this.client_id}&client_secret=${this.client_secrate}`);
        const repo = await repoResponse.json();   

        return {
            profile,
            repo
        }
    }
}