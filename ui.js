class UI {
  constructor() {
    this.profile = document.getElementById('profile');
  }

  profileView(user) {
    this.profile.innerHTML = `
        <div class="card card-body mb-3">
          <div class="row">
            <div class="col col-md-3">
              <img class="img-fluid mb-3" src="${user.avatar_url}" alt="user profile image">
              <a href="${user.html_url}" class="btn btn-success ml-5" target="_blank">View Profile</a>
            </div>
            <div class="col col-md-9">
              <span class="badge badge-primary">Public Repo: ${user.public_repos}</span>
              <span class="badge badge-secondary">Public Gists: ${user.public_gists}</span>
              <span class="badge badge-success">Public Followers: ${user.followers}</span>
              <span class="badge badge-warning">Public Following: ${user.following}</span>

              <ul class="list-group mt-3">
                <li class="list-group-item">Company: ${user.company}</li>
                <li class="list-group-item">Website/Blog: ${user.blog}</li>
                <li class="list-group-item">Email: ${user.email}</li>
                <li class="list-group-item">Location: ${user.location}</li>
              </ul>
            </div>
          </div>
        </div>
      
        <h3 class="page-heading mb-3">Latest Repo</h3>
        <div class="repos"></div>
    `;
  }

  responseRepos(repos){
    let output = '';

    repos.forEach((repo) => {
      output += `
          <div class="card card-body mb-2">
            <div class="row">
              <div class="col col-md-6">
                <a href="${repo.html_url}" target="_blank">${repo.name}</a>
              </div>
              <div class="col col-md-6">
               <span class="badge badge-success">Stars : ${repo.stargazers_count}</span>
               <span class="badge badge-secondary">Watchers : ${repo.watchers_count}</span>
               <span class="badge badge-primary">Forks : ${repo.forks_count}</span>
              </div>
            </div>
          </div>
      `;
    });
    //ouput the repos
    document.querySelector('.repos').innerHTML = output;
  }

  showAlert(message, className){
    this.clearAlert();
    //create a div
    const div = document.createElement('div');
    //added a class
    div.className = className;
    div.appendChild(document.createTextNode(message));
    //find conatainer
    const container = document.querySelector('.searchContainer');
    const search = document.querySelector('.search');
    //set alert
    container.insertBefore(div, search);
    //setTimeout
    setTimeout(() => {
      this.clearAlert();
    },2000);
  }

  clearAlert(){
    const currentAlert = document.querySelector('.alert');
    if(currentAlert){
      currentAlert.remove();
    }
  }

  clearProfile(){
    this.profile.innerHTML = '';
  }
}